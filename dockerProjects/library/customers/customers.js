require('./Customer');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const port = process.env.PORT;
const Customer = mongoose.model('Customer');

app.use(bodyParser.json());

mongoose.connect('mongodb://jctorres:claroIndra@localhost:27017/customerservice', { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log('Database is connected');
});

app.get('/customers', (req, res) => {
    Customer.find().then((customers) => res.json(customers)).catch((err) => { throw err })
});

app.get('/customer/:id', (req, res) => {
    Customer.findById(req.params.id)
        .then((customer) => res.json(customer))
        .catch((err) => { throw err })
});

app.delete('/customer/:id', (req, res) => {
    Customer.findByIdAndDelete(req.params.id)
        .then((req, res) => res.send('Usuario borrado'))
        .catch((err) => { if (err) throw err })
})

app.post('/customer', (req, res) => {
    let newCustomer = {
        name: req.body.name,
        age: req.body.age,
        address: req.body.address
    };

    let customer = new Customer(newCustomer);
    customer.save().then(() => {
        const resMessage = 'New customer created';
        console.log(resMessage);
        res.send(resMessage);
    })
        .catch((err) => { if (err) throw err })
})

app.listen(port, () => {
    console.log('Customer service is running in port', port);
})