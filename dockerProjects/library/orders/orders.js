require('./Order');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const port = process.env.PORT;
const customerHost = `http://localhost:${process.env.CUSTOMER_PORT}/`;
const bookHost = `http://localhost:${process.env.BOOK_PORT}/`;
const Order = mongoose.model('Order');
const axios = require('axios');

app.use(bodyParser.json());

mongoose.connect('mongodb://jctorres:claroIndra@localhost:27017/orderservice', { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log('Database is connected');
}).catch((err)=>{throw err});

app.listen(port, () => {
    console.log('Microservicio de ordenes corriendo en', port);
    console.log('Microservicio de customers corriendo en', customerHost);
    console.log('Microservicio de books corriendo en', bookHost);
});

app.get('/orders', (req, res) => {
    Order.find().then((orders) => res.json(orders)).catch((err) => { if (err) throw err })
});

app.get('/order/:id', (req, res) => {
    Order.findById(req.params.id).then(async (order) => {
        let localOrder = {  };
        if (order) {
            console.log(`${customerHost}customer/${order.CustomerId}`);            
            await axios.get(`${customerHost}customer/${order.CustomerId}`)
                .then((customer) => {
                    localOrder.customer = customer.data.name;
                    console.log('localOrder',localOrder);
                    
                }).catch((err)=>{
                    console.log('Error en customer',err);
                });
            await axios.get(`${bookHost}book/${order.BookId}`)
                .then((book) => localOrder.book = book.data.title).catch((err)=>{
                    console.log('Error en book',err);
                });;
            res.json(localOrder);
        } else {
            res.send("Orden inválida");
        }
    });
});

app.post('/order', (req, res) => {
    console.log('body', req.body);
    let newOrder = {
        CustomerId: mongoose.Types.ObjectId(req.body.CustomerId),
        BookId: mongoose.Types.ObjectId(req.body.BookId),
        initialDate: req.body.initialDate,
        deliveryDate: req.body.deliveryDate,
    }

    let order = new Order(newOrder);
    order.save().then(() => res.send('Orden creada')).catch((err) => { if (err) throw err })
})


