const mongoose = require('mongoose');

mongoose.model('Order', {
    CustomerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    BookId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    initialDate: {
        type: Date,
        required: true
    },
    deliveryDate: {
        type: Date,
        required: true
    }
})