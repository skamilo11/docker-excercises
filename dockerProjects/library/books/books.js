const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const port = process.env.PORT;
require("./Book");

app.use(bodyParser.json());

mongoose.connect('mongodb://jctorres:claroIndra@localhost:27017/bookservice', { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log('Database is connected');
});

const Book = mongoose.model('Book');

app.get('/', (req, res) => {
    res.send("This is our main endpoint");
});

app.get('/books', (req, res) => {
    Book.find().then((books) => res.json(books)).catch(err => { throw err });
});

app.get('/book/:id', (req, res) => {
    Book.findById(req.params.id).then((book) => res.json(book)).catch((err) => { throw err });
});

app.delete("/book/:id", (req, res) => {
    Book.findByIdAndDelete(req.params.id).then(()=>console.log('deleted')).catch((err)=> console.log('err',err));
    res.send('deleted');
});

app.post('/book', (req, res) => {

    var newBook = {
        title: req.body.title,
        author: req.body.author,
        numberPages: req.body.numberPages,
        publisher: req.body.publisher
    }

    //Create a new book 
    var book = new Book(newBook);
    book.save().then((i) => {
        const resMessage = 'New book created';
        console.log(resMessage);
        res.send(resMessage);
    }).catch((err) => {
        if (err) {
            throw err;
        }
    });

    console.log(req.body);
});



app.listen(port, () => {
    console.log('This is our Books service running in port: ' + port);
});